#!/bin/bash
mkdir ../../data/formatted
for data_file in ../../data/extracted/*
do
    file_name=$(echo ${data_file} | awk -F"/" '{print $5}') 
    echo $file_name
    #s/:/,/g ;
    sed 's/"//g; s/\[//g ; s/\]//g ; s/ /,/g  ; s/\// /g ; s/:/,/g ; s/+/,/g' ${data_file} > ../../data/formatted/${file_name}.csv &
    pids[${i}]=$!
done

for pid in ${pids[*]}; do
    wait $pid
done
echo "All formatting finished"